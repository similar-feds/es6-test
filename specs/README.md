# Steps Navigator

Your task, is to build a step navigator, that will show any number of predefined steps.
Each step, has its own definition data, in the following syntax:


```
#!json

 {
    "id": "level-1",
    "title": "Level 1",
    "points": 0,
    "progress": 0
},
{
    "id": "level-2",
    "title": "Level 2",
    "points": 5,
    "progress": 25
},
{
    ...
}
```

Each Task, shows the number of points achieved, progress number, and progress bar.
Here are screenshots of all 5 steps:

[screenshots](SCREENS.md)

## Task 1

Develop a UI Component that can receive the step data defined in [steps data](/src/js/config/steps.js)
and render the steps visually as defined in the [screenshots](SCREENS.md) (but without the animations for now)

Moving between the steps is done by clicking on one of the five small circles at the bottom, 
as seen by this [video](steps-anim.mp4) 

You have the starter styles in [main.scss](/src/css/main.scss), as well as the progress svg
and the colors for each step.

## Task 2

Add animations to the steps, as shown in this [video](steps-anim.mp4)

1. Page background transition, with `0.75s` duration and timing function of `cubic-bezier(.75, 0, .1, 1)`
1. Progress transition, with `1s` duration and timing function of `cubic-bezier(.75, 0, .1, 1)`
1. Step point numbers transition like in the video

The animations are already defined in the [animations](/src/css/_transitions.scss)

## Guidelines

1. **Do not** use any external libraries (NO angular or react).
You must write all the code yourself (but you can get ideas from anywhere).

1. You can assume that the code will run on the **latest chrome version**.
No other browsers need to be supported.

1. Your code can take advantage of the latest ES6 features, 
and any ES7 features already supported by babel transpilers.

1. The [app.js](/src/js/app.js) is the main code entry point.
Any components need to be imported in it.

1. The [main.scss](/src/css/main.scss) is the main SCSS entry point.
Any style modules need to be imported in it.

1. Create components to maximaize code reuse and separate responsibilities.
You can look at the implementation of the `<Message>` component for ideas- but all the options are open.

1. Separate presentation components from logic components.
 
4. Once you are done with the code, follow the instructions for submitting the code.

## Submitting the code

To submit the code please do the following:

1. Commit your changes locally (if you did several local commits, 
you can [squash](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History#Squashing-Commits) them to one local commit)

2. Check your commit and make sure nothing is missing. (Don't forget to add any new files!)

3. Create a [patch](https://git-scm.com/docs/git-format-patch) file from the commit. Usually running `git format-patch` will create patch files for the local commits

4. Send us the patch file
