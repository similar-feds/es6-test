# SimilarWeb Test Starter

### ES6 starter using Babel and webpack, with ES6-compiling, automatic reloading dev server.

more about 

- babel: http://babeljs.io/repl/

- webpack: http://webpack.github.io/docs/

- scss: http://sass-lang.com/

### Please go over the installation instructions, and assignment specs

## 01. Install

```
#!sh

git clone https://bitbucket.org/similar-feds/es6-test.git
cd es6-test
npm i
```

## 02. Run

- `npm start` to start webpack dev server in watch mode
- Open [localhost:8080](http://localhost:8080/) in your browser.
- Hack away - your changes will reload the browser automatically 

## 03 Assignment

### The assignment [specs](/specs/README.md) are here.