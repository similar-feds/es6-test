'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: [
        // Add your application's files below
        './src/css/main.scss',
        './src/js/app'
    ],
    output: {
        publicPath: '/',
        path: path.resolve(__dirname, 'src'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',

                // Only run `.js` files through Babel
                test: /\.js$/,

                // Skip any files outside of your project's `src` directory
                include: path.join(__dirname, 'src')
            },

            // Load SCSS
            {
                loader: "style-loader!css-loader?importLoaders=1!postcss-loader!sass-loader",
                test: /\.scss$/,
                include: path.join(__dirname, 'src/css')
            },

            // Load plain-ol' vanilla CSS
            {
                loader: "style-loader!css-loader!postcss-loader",
                test: /\.css$/,
                include: path.join(__dirname, 'src/css')
            }
        ]
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development'),
            'NODE_ENV': JSON.stringify('development')
        }),
        new HtmlWebpackPlugin({
            template: path.resolve('src', 'index.tpl.html'),
            inject: 'body',
            filename: 'index.html'
        }),
        new webpack.NamedModulesPlugin(),
        // prints more readable module names in the browser console on HMR updates
    ],
    devServer: {
        contentBase: path.join(__dirname, 'css')
    },
    stats: {
        // Nice colored output
        colors: true
    },
    // Create Sourcemaps for the bundle
    devtool: 'inline-source-map'
};