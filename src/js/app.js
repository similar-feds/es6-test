'use strict';

/**
 * This is the main js entry point.
 * Any js components need to be imported here.
 */
import Message from "./components/message";
import stepsConfig from "./config/steps";

let messageElem = document.querySelector('Message');
new Message(messageElem).render();


console.log(`reading config: `, stepsConfig);