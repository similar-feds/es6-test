let svg = `<svg class="svg-content" version="1.1" viewBox="0 0 200 200">
    <circle cx="100" cy="100" fill="transparent" r="90" stroke-dasharray="${Math.PI * 180}"></circle>
    <circle cx="100" cy="100" fill="transparent" id="bar" r="90" stroke-dasharray="${Math.PI * 180}" transform="rotate(270,100,100)" style="stroke-dashoffset: ${Math.PI * 90};"></circle>
</svg>`;

export default svg;