/**
 * Message custom component
 */
export default class Message {

    /**
     * init the message
     * @param elem
     */
    constructor(elem) {
        this.elem = elem;
        this.message = elem.getAttribute('name');
    }


    /**
     * Renders the component
     * @returns {string} the component markup
     */
    render() {
        let markup = `Hey ${this.message}. <b>Good luck!</b>`;
        this.elem.innerHTML = markup;
    }
}
