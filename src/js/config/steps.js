let config = {
    "steps": [
        {
            "id": "level-1",
            "title": "Level 1",
            "points": 0,
            "progress": 0
        },
        {
            "id": "level-2",
            "title": "Level 2",
            "points": 5,
            "progress": 25
        },
        {
            "id": "level-3",
            "title": "Level 3",
            "points": 50,
            "progress": 50
        },
        {
            "id": "level-4",
            "title": "Level 4",
            "points": 200,
            "progress": 75
        },
        {
            "id": "level-5",
            "title": "Level 5",
            "points": 500,
            "progress": 100
        }
    ]
};

export default config;